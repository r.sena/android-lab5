package ryansenna.com.lab5;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent i = getIntent();
        displayText(i);
    }

    private void displayText(Intent i) {

        String sharedText = i.getStringExtra(Intent.EXTRA_TEXT);
        TextView results = null;
        if(sharedText != null)
        {
            results = (TextView)findViewById(R.id.resultsView);
            results.setText(sharedText);
        }

    }

    public void launchActivity2(View v){startActivity(new Intent(this,Activity2.class));}
    public void launchActivity3(View v) { startActivity(new Intent(this,Activity3.class)); }
    public void launchWebBrowser(View v) { startActivity(new Intent(Intent.ACTION_WEB_SEARCH));}

    public void launchMaps(View v)
    {

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("geo:0,0"));
        startActivity(i);
    }
}
