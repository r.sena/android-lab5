package ryansenna.com.lab5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by 1333612 on 9/27/2016.
 */
public class Activity2 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
    }

    public void onClickButton1(View v)
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,"Activity 2: One, 1, uno, un/une");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    public void onClickButton2(View v)
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,"Activity 2: Two, 2 , due, deux");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

}
