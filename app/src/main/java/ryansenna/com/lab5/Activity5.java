package ryansenna.com.lab5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by 1333612 on 9/27/2016.
 */
public class Activity5 extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_5);
    }

    @Override
    public void finish()
    {
        String sharedText = "Dave I'm afraid I can't do that.";

        Intent sendIntent = new Intent(this, Activity3.class);

        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,sharedText);
        sendIntent.setType("text/plain");

        startActivity(sendIntent);
    }
}
