package ryansenna.com.lab5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * Created by 1333612 on 9/27/2016.
 */
public class Activity3 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);
        Intent i = getIntent();
        displayText(i);
    }

    private void displayText(Intent i) {

        String sharedText = i.getStringExtra(Intent.EXTRA_TEXT);
        TextView results = null;
        if(sharedText != null)
        {
            results = (TextView)findViewById(R.id.results);
            results.setText(sharedText);
        }
    }

    public void launchActivity4(View v) { startActivity(new Intent(this, Activity4.class));}
    public void launchActivity5(View v) { startActivity(new Intent(this, Activity5.class));}
}
